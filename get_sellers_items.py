# Very early code - imports all items and all sellers into a database

import urllib2
import time
import json
import MySQLdb
from stringOps import escapeQuotes, find, db_query, log, itemNeedsUpdate

sellerData = urllib2.urlopen("https://api.commissionfactory.com/V1/Affiliate/Merchants?apiKey=b5daec5d413241ffbaac31b455102302").read()

dataFeeds = urllib2.urlopen("https://api.commissionfactory.com/V1/Affiliate/DataFeeds?apiKey=b5daec5d413241ffbaac31b455102302").read()

# convert to json
sellerData = json.loads(sellerData)
dataFeeds = json.loads(dataFeeds)
count = 0
total = 0

sellers = []


# add all sellers than have joined the program to a list of sellers
for row in sellerData:
	total += 1
	if (row[u'Status'] == u'Joined'):
		count += 1
		sellers.append(row)
		
print count, total

for seller in sellers:
	seller["dataFeedIds"] = []
	for each in dataFeeds:
		if each[u'MerchantId'] == seller[u'Id']:
			seller["dataFeedIds"].append(each[u'Id'])
for seller in sellers:
	try:
		print seller["dataFeedIds"]
	except:
		print "No items for this seller"
				
# ========== Now save to database				
# Open database connection
db = MySQLdb.connect("localhost","root","","test" )



for seller in sellers:
	query = "SELECT * FROM Seller WHERE SellerID = %d;" % (int(seller[u'Id']))
	data = db_query(query, db)
	# get the important details and escape the quotes so they can be stored in the db
	# also encode them
	name = seller[u'Name'].encode('UTF-8', 'ignore')
	id = seller[u'Id']
	LastModified = seller[u'DateModified']
	summary = seller[u'Summary'].encode('UTF-8', 'ignore')
	URL = seller[u'TargetUrl'].encode('UTF-8', 'ignore')
	TrackingURL = seller[u'TrackingCode'].encode('UTF-8', 'ignore')
	category = seller[u'Category'].encode('UTF-8', 'ignore')
	# seller is not in records, so add them
	# print data
	
	if data == ():
		count += 1
		summary = seller[u'Summary']
		
		# create the add query
		# note: last '''%s''' must be single quoted as it contains a string (it is an <a href "url"> tag)
		add = """INSERT INTO Seller VALUES ('''%s''', '''%s''', '''%s''', '''%s''', '''%s''', '''%s''', '''%s''');""" % (name, id, LastModified, summary, URL, category, TrackingURL)
		db_query(add.encode("latin-1", "ignore"), db)

		
# now add the items

for seller in sellers[:4]:
	items = []
	for id in seller["dataFeedIds"]:
		url ="https://api.commissionfactory.com/V1/Affiliate/DataFeeds/" + str(id) + "?apiKey=b5daec5d413241ffbaac31b455102302"
		log(url)
		# load the data about the item
		itemInfo = json.loads(urllib2.urlopen(url).read())
		for item in itemInfo[u'Items']:
			items.append(item)
	#print items
	
	for item in items:
		
		# check if the item is the same as the one in the database (if not, we will need to update database)
		if itemNeedsUpdate(item, db):
			# Get info for the item =================================
			id = item[u'Id']
			name = item[u'Name']
			lastModified = item['DateModified']
			description = item['Description'].replace("'", "\'")
			im100 = item['Image100Url']
			im120 = item['Image120Url']
			im200 = item['Image200Url']
			im300 = item['Image300Url']
			im400 = item['Image400Url']
			url = item[u'TrackingUrl']
			category = item[u'Category']
			embedUrl = item[u'TrackingCode']
			price = item[u'Price']
			sellerID = seller[u'Id']
			
			print sellerID
			# ======================================================
			
			# create query and then run on db
			add = """INSERT INTO Item VALUES (%d, '''%s''', '''%s''', '''%s''', '''%s''', '''%s''', '''%s''', '''%s''', '''%s''', '''%s''', '''%s''', '''%s''', %s, %s);""" % (id, name, lastModified, description, im100, im120, im200, im300, im400, url, category, embedUrl, price, sellerID)
			try:
				db_query(add.encode("latin-1", "ignore"), db)
			except Exception as inst:
				log(add.encode("latin-1", "ignore"))
				print inst
				exit()


		
# disconnect from server
db.close()
	


		


